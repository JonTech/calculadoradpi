package com.exemple.profedam.calculadoradpi;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.exemple.profedam.calculadoradpi.controllers.MainActivity;
import com.exemple.profedam.calculadoradpi.model.Pantalla;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, true,
                    true);


    @Test
    public void testMainRotate() throws Exception {

        activityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //onView(withId(R.id.btnCalcular)).perform(click());
        onView(withId(R.id.tvDiagonalED)).check(matches(withText("5.0")));

    }

    @Test
    public void testVertical() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("350"), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("20"), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta resolución vertical")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void testHorizontal() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText("400"), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("20"), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta resolución horizontal")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }
    @Test
    public void testDiagonal() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("350"), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText("400"), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText(""), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta Diagonal")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void testVerticalMenosDe_100() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("260"), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText("58"), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("9.0"), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("La resolució vertical ha de ser major a 100")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void testHorizontalMenosDe_100() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("25"), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText("400"), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("9.0"), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("La resolució horitzontal ha de ser major a 100")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void testDiagonalMenosDe_100() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("380"), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText("450"), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("0.5"), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("La diagonal ha de ser major a 1")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }
    @Test
    public void testPerfecto() {
        onView(withId(R.id.etResolucioHoritzontal)).perform(typeText("250"), closeSoftKeyboard());
        onView(withId(R.id.etResolucioVertical)).perform(typeText("450"), closeSoftKeyboard());
        onView(withId(R.id.etDiagonal)).perform(typeText("6.0"), closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withId(R.id.tvDpi)).check(matches(withText("85 dpi")));
    }





    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.exemple.profedam.calculadoradpi", appContext.getPackageName());
    }




}




